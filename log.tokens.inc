<?php

/**
 * @file
 * Token integration for the log module.
 */

/**
 * Implements hook_token_info().
 */
function log_token_info() {
  // Log entry tokens.
  $info['types']['log-entry'] = array(
    'name' => t('Log entry'),
    'description' => t('Tokens related to log entries.'),
    'needs-data' => 'log_entry',
  );
  $info['tokens']['log-entry']['id'] = array(
    'name' => t('Entry ID'),
    'description' => t('The unique ID of the log entry.'),
  );
  $info['otkens']['log-entry']['type'] = array(
    'name' => t('Log entry type'),
    'description' => t('The type of log entry.'),
    'type' => 'log-entry-type',
  );
  $info['tokens']['log-entry']['user'] = array(
    'name' => t('User'),
    'description' => t('The user associated with the log entry.'),
    'type' => 'user',
  );
  $info['tokens']['log-entry']['created'] = array(
    'name' => t('Date created'),
    'description' => t('The date the log entry was created.'),
    'type' => 'date',
  );
  $info['tokens']['log-entry']['changed'] = array(
    'name' => t('Date changed'),
    'description' => t('The date the log entry was most recently updated.'),
    'type' => 'date',
  );
  $info['tokens']['log-entry']['url'] = array(
    'name' => t('URL'),
    'description' => t('The URL of the log entry.'),
    'type' => 'url',
  );
  $info['tokens']['log-entry']['edit-url'] = array(
    'name' => t('Edit URL'),
    'description' => t("The URL of the log entry's edit page."),
  );

  // Log entry type tokens.
  $info['types']['log-entry-type'] = array(
    'name' => t('Log entry types'),
    'description' => t('Tokens related to log entry types.'),
    'needs-data' => 'log_entry_type',
  );
  $info['tokens']['log-entry-type']['name'] = array(
    'name' => t('Name'),
    'description' => t('The name of the log entry type.'),
  );
  $info['tokens']['log-entry-type']['machine-name'] = array(
    'name' => t('Machine-readable name'),
    'description' => t('The unique machine-readable name of the log entry type.'),
  );
  $info['tokens']['log-entry-type']['description'] = array(
    'name' => t('Description'),
    'description' => t('The optional description of the log entry type.'),
  );
  $info['tokens']['log-entry-type']['count'] = array(
    'name' => t('Log entry count'),
    'description' => t('The number of log entries belonging to the log entry type.'),
  );
  $info['tokens']['log-entry-type']['edit-url'] = array(
    'name' => t('Edit URL'),
    'description' => t("The URL of the log entry type's edit page."),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function log_entry_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  $url_options = array('absolute' => TRUE);
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }

  $sanitize = !empty($options['sanitize']);

  // Log entry tokens.
  if ($type == 'log-entry' && !empty($data['log_entry'])) {
    $log_entry = $data['log_entry'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'id':
          $replacements[$original] = $log_entry->id;
          break;
        case 'type':
          $log_entry_type = log_entry_type_load($log_entry->type);
          $replacements[$original] = $sanitize ? check_plain($log_entry_type->name) : $log_entry_type->name;
          break;
        case 'user':
          $account = user_load($log_entry->uid);
          $name = format_username($account);
          $replacements[$original] = $sanitize ? check_plain($name) : $name;
          break;
        case 'created':
          if ($log_entry->created) {
            $replacments[$original] = format_date($log_entry->created, 'medium', '', NULL, $language_code);
          }
          break;
        case 'changed':
          if ($log_entry->changed) {
            $replacments[$original] = format_date($log_entry->changed, 'medium', '', NULL, $language_code);
          }
          break;
        case 'url':
          $uri = entity_uri('log_entry', $log_entry);
          $replacements[$original] = url($uri['path'], $uri['options'] + $url_options);
          break;
        case 'edit-url':
          $replacements[$original] = url("log/entry/{$log_entry->id}/edit", $url_options);
          break;
      }
    }

    // [log-entry:user:*] chained tokens.
    if ($user_tokens = token_find_with_prefix($tokens, 'user')) {
      $account = user_load($log_entry->uid);
      $replacements += token_generate('user', $user_tokens, array('user' => $account), $options);
    }
    // [log-entry:created:*] chained tokens.
    if ($log_entry->created && $created_tokens = token_find_with_prefix($tokens, 'created')) {
      $replacements += token_generate('date', $created_tokens, array('date' => $log_entry->created), $options);
    }
    // [log-entry:changed:*] chained tokens.
    if ($log_entry->changed && $changed_tokens = token_find_with_prefix($tokens, 'changed')) {
      $replacements += token_generate('date', $changed_tokens, array('date' => $log_entry->changed), $options);
    }
    // [log-entry:url:*] changed tokens.
    if ($url_tokens = token_find_with_prefix($tokens, 'url')) {
      $replacements += token_generate('url', $url_toekns, entity_uri('log_entry', $log_entry), $options);
    }
  }

  // Log entry type tokens.
  if ($type == 'log-entry-type' && !empty($data['log_entry_type'])) {
    $log_entry_type = $data['log_entry_type'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'name':
          $replacements[$original] = $sanitize ? check_plain($log_entry_type->name) : $log_entry_type->name;
          break;
        case 'machine-name':
          // This is a machine name so does not ever need to be sanitized.
          $replacements[$original] = $log_entry_type->type;
          break;
        case 'description':
          $replacements[$original] = $sanitize ? filter_xss($log_entry_type->description) : $log_entry_type->description;
          break;
        case 'count':
          $query = db_select('log_entry');
          $query->condition('type', $log_entry_type->type);
          $query->addTag('log_entry_type_count');
          $count = $query->countQuery()->execute()->fetchField();
          $replacements[$original] = (int) $count;
          break;
        case 'edit-url':
          $replacements[$original] = url("admin/structure/types/manage/{$node_type->type}", $url_options);
          break;
      }
    }
  }

  return $replacements;
}
